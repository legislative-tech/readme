<!-- This template is for use if you'd like to participate in the project more directly than simply submitting merge requests from your external fork -->

### I want to help by

<!-- Share how you want to help the projects or the organization. -->

### Examples

<!-- Share examples of past work or hypotheticals for what you'd bring to the organization. -->

### Resources requested

- [ ] Chat service
- [ ] Email 'username'@legislative.tech address
- [ ] GitLab project issues
- [ ] GitLab project developer access
- [ ] GitLab project approver / merge
- [ ] GitLab project maintainer
- [ ] Non-production infrastructure
- [ ] Production infrastructure

/confidential