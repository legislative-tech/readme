---
title: "About Legislative.Tech"
#subtitle: ""
date: '2021-02-01'
description: "This is meta description"
draft: false
---
By focusing on specific issues, systems thinking, and local politics, we can turn the temperature down on partisan rhetoric and love our neighbors again. 

#### The ultimate goal is for everyone to be heard, disagree, and then move forward to improve our lives collectively.

The Legislative.Tech banner is intended to make the impact of this effort bigger than any individual. 

## Motivation

As a regular citizen, try to get your local representative to hear what you're asking for and prioritize it effectively. Sending an email, cornering them at a townhall, or calling their office is rarely satisfying unless they were already going to vote exactly as you intended. Life and law are too complex and impacts different people in different ways, and our capacity for empathy and community is limited.
As Lynn Terhar said, "It's shocking how few people make legal choices and how focused their important goals are."

*  The elected officials at the state level have other things to do, it's not a full-time job since it doesn't pay a fulltime wage
*  The federal level representatives and senators are responsible for millions of people and human imagination and empathy can't process that
*  Despite representatives being on the same team as a constituent, they may be on that time for a different reason
*  People run for office because they're passionate about something but the limits of psychology prevent someone from being passionate about every piece of legislation

It would be nice to have a dispassionate representative who was informed clearly, quantitatively, about what their constituents care most about.

The last leg of this effort is to redirect lobbying so that instead of trying to convince and individual to support your initiatives legislatively, the company would bring their case to the districts that it affects. If everyone in the district has to drink flammable water, they may be less likely to support deregulation. If dozens of small businesses in a community will benefit from destroying a bird sanctuary, it would draw out more nuanced discussions.

Of course, the person casting votes still has a conscious and responsibility to anticipate the needs of the minority or underrepresented voices so votes will not match the majority's desires. Tyranny of the majority requires human intuition to overcome.

## The first steps

Make two applications that mediate interactions between congressmen and their constituents. The goal is to have an easy and engaging user experience for both parties. If a high level of satisfaction can't be reached, the apps need to be reimagined. 

The first really difficult problem to solve is to map online personas to real humans in real districts. Data sources need to be collected or correlated and updated continuously. People move, district lines are redrawn as populations move around.  **Districts.app should address this.**

The second really difficult problem to solve is understanding individuals' priorities and coalescing them into something that helps them feel heard. Creating classifications for things to have opinions about and then ranking "very yes" to "very no" is typically dehumanizing and surveys with dozens or hundreds of questions to answer are often leading and ignore the needs for people with language difficulties. **MyRep.app should address this.**

[Check out the Software Page For more information](/software)

## Values

The articles of incorporation and bylaws include several constraints that reflect practical implementation of some of these values that have directly applicable interpretations. The list here is more of an aspirational list of ways that we want to work, which we acknowledge will not be possible in all situations. These values will also conflict in many ways so identifying the more important value to apply in a situation will differ by perspectives. 

### S. I. C. K.

Our values are `sick`, bro.

*  Systems thinking
*  Increments
*  Community
*  Kindness

When running the organization, building the software, engaging with users, donors, and the outside world, we strive to always use these as the framework.

#### Systems thinking

This is the first because it's the toughest for people to do. Nobody does it naturally. It requires observing carefully to understand a situation. Reviewing what was explicitly stated and what was implied and assumed. Dig into each implication and assumption to identify correlation and causal relationships. Using this to synthesize a bigger picture that informs what the best steps are to take, rather than the most obvious. Focus on leverage points and feedback loops so that outcomes tend toward the intention while avoiding the system traps. 

For more, see [Thinking in Systems by Donella Meadows](https://smile.amazon.com/Thinking-Systems-Donella-H-Meadows/dp/1603580557/)

#### Increments

This value was borrowed from [GitLab's Iteration Value](https://about.gitlab.com/handbook/values/#iteration), though the iteration value focuses on keeping throughput high. We can take the same approach but with less focus on throughput and more focus on taking the next right step. Even if it seems like less progress than an alternative approach. Avoiding technical debt or tapping into the right psychological mechanisms using the best design should be prioritized over shipping something that costs social capital and requires rework. 

It's good to be small, but must be in the right direction.

#### Community

This effort only gets started and will only succeed if we have a strong community surrounding the work. The amount of work required, the variety of expertise will be tough to wrangle, the resources to keep it running will grow.  

The foundation of the software created by Legislative.Tech is Open Source, created and supported by the open source community. 

To live the community value, be vulnerable, ask for help, and provide help when it's needed.

#### Kindness

The best way to lose a community is by being rude to them or behaving in exclusionary ways. The diversity, inclusion, and belonging efforts all fall into understanding and prioritizing kindness. People learn when they experience pain, whether you want them to or not. If they learn that submitting ideas or code or models or volunteering to help is a painful experience due to rude, dismissive, or other antisocial behaviors, they won't do it again. Even if they really believe in the mission and want to help. 

Kindness doesn't mean accepting bad code or being nice to trolls or tolerating hate speech or abusive behavior from the community. It's about being polite and positive and assuming positive intent. 

Kindness is a two-way street as well, when people from the community contribute, if they are rude about the contribution, they will be reminded of this value. 

> It is more kind to tell a person that they have food in their teeth than to let them go through a whole day unaware of it. 

### Reinforcement

Anybody should feel comfortable pointing to the values at any time if they feel someone is acting in opposition to those values. 

This can be in the form of a brief statement such as "This source code contribution is appreciated but contains too many changes which goes against our Increments value. Please dissect it into smaller components to expedite review and acceptance." Include a link so that people can read and react accordingly. 

If you are called out on a value, consider it an act of kindness on the part of the individual who called you out. It's not easy to do that but it enables better collaboration. 

