# Tech Stack

This page outlines all of the technologies and vendors used by the organization. The intention in sharing this is to facilitate transparency and let contributors, volunteers, and users have visibility into how the team operates. 

Legislative.Tech is a mostly-online entity so without any local devices and servers, all of the data processing, storage, and handling are done with SaaS and cloud vendors. 

## Third Parties 

Vendor | Purpose | Data
---    | ---     | ---
GitLab | Collaboration platform | Source code, issues, users, hosted pages
Fathom | Web site usage         | Count of users accessing pages, list of referrers
Honeycomb | Application telemetry | Traces of user activity in applications built by Legislative.Tech, 30-day retention
