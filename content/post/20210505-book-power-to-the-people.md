---
date: 2021-05-05
linktitle: 'Power to the Public is a Must-Read'
title: 'Book: Power to the Public is a Must-Read'
categories: [ "general" ]
image: '/images/posts/20210505-power-2-people-sq.jpg'
tags: ["book" ]
weight: 10
draft: false
type: "regular"
---
Thanks to Obama tweeting about a the book, I was immediately interested in reading this to see what elements of technology and public policy were brought up and how they were related. Having been involved in the execution of so many government projects in the past, I really wanted to see if the authors were able to capture the frustration and helplessness that public servants and the public both experience. 

## They sure did!

The authors' backgrounds are in public policy and technology but from different angles. Their focus ties three major "ways of working" that the private sector has adopted which the public sector is resisting. Their examples range from USCIS to the IRS to Vermont's benefits programs and the cycle goes something like this:

1.  A policy has a good idea behind it
2.  The people implementing it do so based on what makes it easy on the bureaucracy
3.  The people who should be benefiting from it are blocked or impeded
4.  Someone boldly violates the bureaucracy's structures to deliver better service
5.  The scope is kept small so it can be labeled a success

Sufficiently large programs can't be holistically tackled due to the systems that are built to deliver them to the public. The incentives are incredibly misaligned and things like avoiding fraudulent benefits are prioritized way over delivering timely benefits to those who need them. This is all called out in the book in a series of great examples.

## COVID relief legislation example

One section talks about how the CARES act for the first round of COVID relief was drafted and passed.

#### Paraphrasing a nuanced discussion as a list

*  Congress has no way to survey their constituents rapidly
*  half the people who needed checks didn’t get them for a variety of reasons
*  this mainly impacted people on the fringes of society who don't have consistent addresses and employment

The core assumption is that money comes in through the IRS so they're in the best position to send it out. That may be true for the model citizens who live and work a consistent lifestyle and were not immediately facing stability issues due to the pandemic. Anyone without direct deposit or stable housing (which the pandemic may have created more of an issue) was delayed or blocked from the stimulus.

There are interesting things to unpack on the legislative side, but the main point they made was this:

> If lawmakers had experts or direct access to the people who needed it most, they could have written a better law.

#### Yes!

When a policymaker needs to make a good policy rapidly, the more information they have available to them the better they can do. By creating policies that leave room for implementation details to be worked out by the executive, the design and data principles can be used to improve delivery. If the policy tries to be prescriptive for the details, it will struggle and fail to meet the needs of the citizens.

{{< figure src="/images/posts/20210505-power-2-public-cover.jpg" title="Power to the Public" caption="Public policy meets technological advancements since the 1960s.">}}

## How is this related to Legislative.Tech?

The book is highly relevant to Legislative.Tech in a few ways:

1.  Power to the Public points to the same 3 disciplines that Legislative.Tech is leveraging
1.  Both are about creating better alignment between the people and the government
1.  The core issue driving both is that things needed to work certain ways in the past and don't have to anymore

It is different in a few ways:

1.  Legislative.Tech is about the people-to-representative interface and this book focuses on the services-to-people interface
1.  Here we want to form a consistent channel for communication and there they focus on collecting the feedback from design and data
1.  Legislative.Tech strives to address lobbying and policy flaws and not just implement the flawed policies better

## Parallel feedback loops

{{< figure src="/images/posts/20210505-power-2-people-diagram.png" title="Separate feedback loops" caption="The left feedback loop is where Legislative.Tech focuses, the right feedback loops is where Power to the Public focuses.">}}

Both of these two feedback loops are necessary for good government. The brown, executive side feedback loops can be _much_ faster so it should be used for ironing out details. The purple, legislative side feedback loops can be _much_ more impactful which requires them to move more slowly. 

I want people to understand that the full loop of passing a law, interpreting it, implementing and delivering it, seeing the outcome, getting feedback to voters, changing elected representatives, and then having them pass a new law ... that's a decade-long process. It should only be used for things that change the nature of how our society operates. 

This diagram is also one that has small cycles playing out in cities and towns, medium sized cycles in states, and gigantic cycles in the federal government. Making the local community leadership more reflective of the population creates a good foundation to stabilize and balance the larger, slower cycles.

Legislative cycle should focus on "what we want to do" and executive cycle should focus on "how to do it best". Right now laws come out baked in with details of how it needs to be done which damages the policies. Additional trouble comes from executives who want to undermine a policy or an entire department which requires intervention by legislators or courts. These dysfunctions need to be addressed by the people via oversight. 

## What now?

Check out the book and follow the authors on twitter.

*  [Book on Princeton Press](https://press.princeton.edu/books/ebook/9780691216638/power-to-the-public)
*  [Tara Dawson McGuinness on Twitter](https://twitter.com/taradmcguinness)
*  [Hanna Schank on Twitter](https://twitter.com/hanaschank)
*  [Technical view from Brownfield.dev](https://brownfield.dev/post/2021-04-27-book-power-to-the-public/)

My main takeaway: I'm not alone in seeing these deficiencies and the specific tools to remedy them. This is quite exciting!