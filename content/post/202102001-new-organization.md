---
date: 2021-02-01
linktitle: Introduction Legislative.Tech 
title: Introducing Legislative.Tech 
categories: [ "general" ]
image: "images/bradford-uk.jpg"
tags: ["purpose", "nonprofit" ]
weight: 10
draft: false
type: "regular"
---
Legislative.Tech is founded as a nonprofit organization intended to help bridge the gap between constituents and legislators in a representative democracy. After seeing the country deteriorate into factions in the past decade, something has to be done. The only way out of this reinforcing death spiral is to have a better idea of what we all have in common. 

    This post is a personal account related to the founding of the organization. Any stated or implied political or policy positions should only be attributed to the author and not the organization. To be clear, the organization is not promoting a political agenda. 

As a technologist, the best way I saw to achieve this and ultimately deradicalize people, is to bring them together with certain constraints that limit the psychological tricks that lead to tribalism. From the perspective of someone born at the transition of Generation X to Millennials, I grew up a bit without the internet and then a bit more with it. I saw elders transition from fearful of the internet to completely trusting of it. 

The most recent change that inspired action was that the heavily armed anti-government contingent among the citizens were a deterrent from overstepping government. The supporters of the administration that ended in 2021 proved that these folks were not actually anti-government at all and would actually support authoritarian efforts if they seemed to be on the same team. 

### Teams are meaningless

The fact that I was brought up on one team and largely agree with that team is likely the same for people on the other side as well. We have a few things where our personal experience doesn't align with the party thinking, but since it's mostly okay, it's better to stay with what I know than risk the other team. 

At the polling place in 2018 I had a visceral reaction to a neighbor that I know from down the street talking about how excited he was to support a republican candidate who was against homosexual and transsexual interests. Knowing that people in our neighborhood had these lifestyles and that their vote would directly negatively impact their neighbors was repulsive. 

On reflection later, some of the policies that "my team" support would probably increase taxes on this guy's small business or require him to pay his employees more. He sees that as a threat to his livelihood and possibly resents anybody who would vote "against him and his small business."

This is the false dichotomy that we are forced into with the party system as it exists. It seems designed to create an environment where the individuals act like a bucket of crabs, voting against each other, and end up preventing progress. 

### Who are you to fix this?!

I'm just someone who has some technical skill and has developed a lot of empathy for a lot of people with different backgrounds. Empathy is magic, so I'm going to try to create a digital empathy engine that can help others see individual issues and vote against their parties when appropriate. 

### What now?

[Contribute](/contribute) to the efforts. They're brand new. It's vapor and ideals right now. I need a lot of help. 
