---
date: 2021-02-01
linktitle: Recent Experiences with Contacting Representatives
title: Recent Experiences with Contacting Representatives
categories: [ "general" ]
image: '/images/anonymous-yelling.jpg'
tags: ["problems" ]
weight: 10
draft: false
type: "regular"
---
Recently reached out to my federal, House of Representatives congresswoman and received an uninspiring response. 

    This post is a personal experience and how it relates to the organization. Any stated or implied political or policy positions should be attributed solely to the author.

Firstly, I agree with most of the current legislator's platform. When reaching out, I include praise and support for her efforts to date. 

A few weeks ago, I sent a message supporting a political tactic that has been effective but may draw criticism. The main point was that so many attempts to undermine democracy recently failed, but without consequences, the perpetrators are emboldened to try even more extreme measures. 

This isn't particularly related to any given legislation, but more of a general thing to keep in mind when working with peers and in committee settings. 

{{< figure src="/images/posts/20210219-wexton-email.jpg" title="Generic form response" caption="The wording is generic and not applicable to the type of message that was sent.">}}

Opening the form response of "Thanks for sharing your priorities and will consider it during future legislation" was not satisfying. Something about her reservations about using the same methods as those undermining democracy would have been appreciated, unless she's aligned with the idea and would fully support a more aggressive approach. I really wanted to know what was slowing down the efforts and why she wasn't championing any. 

I believe a system like MyRep.app could have clumped this feedback among many other messages and similar sentiments to provide a constituency-wide topic to respond to more clearly. 

It's also possible that she has a statement out already that addresses it and I just didn't see it. MyRep would let the robot or other constituents show that alignment already exists. 

### Another popular / unpopular politician's twitter thread

As polarizing as [@AOC](https://twitter.com/AOC) is, this thread resonated with me as a deficiency in the system. 

{{< figure src="/images/posts/20210219-aoc-twitter.jpg" title="Representative Ocasio-Cortez Requesting Feedback" caption="The mantra: write and call and reach out to your representative even though we can't respond.">}}

The suggestion is to spend countless human-hours across the district screaming into the void to support a politician that is already doing a thing. I completely agree that this feedback is needed and the request for more of it elicits sympathy on many levels. 

The most direct is as a customer interface to a product team, the customer feedback is largely negative. Nobody is shy about sharing things they dislike. It's easy to criticize. Motivated by frustration, people often take action. 

When the opposite is true, a customer is delighted by a feature, they experienced the reward trigger already and are more likely to use the feature again or explore further. They are not motivated to seek catharsis by yelling at someone or even open a communication channel to passively agree.

This is why many tech companies collect telemetry data so they can see when users are just fine. If the product team doesn't know a feature is good for 80% of users, the noise created by the 20% will drive the product to change and possibly be worse for the majority. 

Adding telemetry to people's lives is a terrible idea, so the next best one is to run a series of micro-surveys on many topics that people can respond to easily. User experience and the reward trigger of feeling heard and agreed with will bring people back for more. 

## What now?

[Contributez](/contribute) to the efforts. They're brand new. It's vapor and ideals right now. I need a lot of help. 
