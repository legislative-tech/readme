---
date: 2022-02-27
linktitle: Re-Introduction Legislative.Tech 
title: Re-Introducing Legislative.Tech 
categories: [ "general" ]
image: "images/clear-sky.jpg"
tags: ["purpose", "nonprofit" ]
weight: 10
draft: false
type: "regular"
---
Legislative.Tech was initially created as a non-profit based on a likited understanding of how the IRS decides what a charity is.  Throughout 2021, we expressed what we were trying to accomplish and how we felt it met the requirements for 501(c)(3) status. The responses focused on how "software" is for "profit" so it was unlikely to be granted. 

To avoid undue constraints going forward, we withdrew the request for charity status and terminated the non-profit corporation. The organizing documents were very restrictive and only useful if offset by the tax benefits and access to charity services. 

## Not giving up

Going to continue working out how to achieve the goals without an organization. It was premature to establish as a charity without a track record of behaving like one. Reverting to the principle of making incremental progress, we will incorporate when theres a need. Until then we will rely on open source licensing and minimal funding. 

## More options 

Without having any organizational structure or bylaws, we can do whatever. The only thing keeping us on track is the participants deciding the best steps. 

### International 

Not being a US-based charity also allows more international participation. 

### Political activity

Nonprofits are restricted from particiapting in certain ways from political activities. Without that constraint, we could dogfood the systems by working closely with candisates and elected officials.

## Lets go!

The sky is the limit now. [Let's get started.](/contribute)