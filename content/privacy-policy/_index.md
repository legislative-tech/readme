# Privacy Policy

Privacy is a cornerstone of engaging in a free and open dialog to build a better democracy. 

## Anonymous visitors

Legislative.Tech's marketing website does not use invasive tracking mechanisms and doesn't store any cookies. For marketing and user experience purposes, [Fathom analytics](https://usefathom.com/ref/5GISEH) counts users access to pages and where they came from. It doesn't include any PII or user agents or other invasive content. The data points are focused on website user experience rather than the individual visitors. 

## Contributors

Contributing to the software or organization via email, chat, gitlab content, or source code creates a relationship between Legislative.Tech and the contributor. Contact information and the nature of the contributions will be stored in the systems where the contributions are made. Due to the transparent nature of the organization, these pieces of information will be publicly accessible or referenced from publicly accessible locations. 

Removal of some of this information may be difficult, especially as a git contribution which can be cloned and forked by unknowable individuals and organizations. Before submitting a contribution, please ensure that any of that information being made public is acceptable. 

## Volunteers

Volunteering time and effort has the same outcome as contributors. Any content created or modified as a volunteer will include meta information about who is making the changes. 

## Donors

Contributing resources to the organization comes with the expectation that your information will be made public. This is a countermeasure against corruptive influcnes. An entirely anonymous donation is acceptable. If the organization is aware of the donor, the publicly available information will include that donor. 

### Financial data 

Financial information is not stored by Legislative.Tech. Credit card donations, even recurring, are stored by the credit card processing vendor. Donations made by check are stored in the banking system as images but not on any Legislative.Tech systems. Cryptocurrency donations are recorded in the blockchain or respective ledgers. 

## Third party sharing

User data, metadata, or derived data is not sold to third parties. Since Legislative.Tech uses cloud or SaaS vendors for everything, the data is processed by and stored in these third parties. The data is subject to their retention and handling policies. 

Legislative.Tech takes vendors' data handling policies into account when selecting a vendor. A chief concern is the privacy of our members, volunteers, contributors, and users. Refer to the [tech stack page](/about/tech-stack) for information on which technologies and vendors are in use at any given time. 

## For more information

The privacy policy is an evolving policy which requires updates as Legislative.Tech makes changes to how it operates. For more information, requested updates, or other comments, contact [info@legislative.tech](mailto:info@legislative.tech) or open an issue in [the repo](https://gitlab.com/legislative-tech/readme/-/issues/new). 
