---
title: "Software"
---

# MyRep.app

The application will have the user interfaces used by both the constituents and representatives to share and consume content. The core belief driving creation of this is that a mixture of statistics (metrics) and anecdotes (art) from a district should be shared among the constituency to drive the policy decisions of a legislative representative. 

#### Goal: Constituents feel heard; Representatives are informed and accountable

Methods for accomplishing these will evolve over time. 

Inputs:

*  (Anecdotes) Constituents submit stories of their experiences
*  Upcoming legislation for the districts
*  (Statistics) Expert opinions, statistics, and other sources are linked to upcoming legislation

Outputs: 

*  (Informed) Constituents coalesce into groups who support or detract from individual policy actions
*  (Accountable) Legislators review inputs submit intentions for upcoming votes
*  The need for new legislation or local solutions to local problems

Connecting inputs to outputs requires building a graph of people's experiences to legislation. Taking these relationships and quantifying them in a way that shows support or aversion and by how much. 

### Privacy 

The most important part of this software is that it takes the constituents wishes into account in terms of how far their stories and experience go. If someone is sharing a personal story of loss related to a legislative topic, maybe they are okay with sharing it with the state legislature or county government but wouldn't want the whole world to have it.  They may also wish for the current representative to be aware of the story but after a year no longer want it factored into decisions. 

The best way to do this would be with a cryptographic solution where the story is saved into an encrypted data store. During initial processing, it is evaluated and tied to topics, added to the graph, and reference for legislation. If the user decides to withdraw the story, they can remove the decryption key from their keychain. The meta information is still available but the story content is rendered unreadable. 

### Authentication

Due to the complexity of tying humans to locations in the real world, and tying humans to online personas, MyRep.app won't even try to solve that problem. 

### Reputation

With large scale and political topics, trolls and exaggerated rhetoric are bound to appear. When an account is using hate speech or threatening violence, the other users can flag them. Self-moderation is the best first step, however it can be abused as users offend each other. Appeals processes and district-based review boards may be required eventually. 

### Algorithms and Transparency

These two aspects need to operate as a pair due to the dangerous nature of algorithms operating in the dark. The mapping and graphing of stories and statistics to laws will need to be done in an automated way for the most part. As stories are processed, the outputs of these evaluations are presented back to the users. If the story is misunderstood or the processing mechanism can't make sense of it, the user can adjust the classifications manually. 

If a user refuses to engage in a dialogue, states they cannot adapt their opinions to new information, or is engaging in bad-faith arguments, the system will inform them that this has been identified. Since users are not classified, it will only impact the relationship between a user and a topic. The user will confirm:

> I am unwilling to take reasoned arguments and objective truth into my opinion of this topic. I do not wish to be presented with any further legislation or stories on this topic.

### What MyRep Isn't

MyRep should never be a system that: 

*  Classifies users or humans
*  Requires consensus to move forward
*  Requires a majority to take an action
*  Spreads or platforms hate speech 
*  Endangers individuals for sharing stories
*  Uses "progressive" or "conservative" to describe anything

# Districts.app

The application will _somehow_ use established district boundaries and individual addresses to figure out who should have access to which parts of the MyRep application. 

#### Goal: Provide a good way to map districts to people to an online identity used by MyRep.app

While existing solutions to this problem are all over the place, none really make sense for this application. Driver records exclude kids and elderly or people who opt not to have cars. Voter registration is better, but a representative represents non-voters and people who are not allowed to vote such as children. 

Some ideas: 

*  Allow individuals to "super prove" who they are and validate others (like a notary system)
*  Use a Know Your Customer (KYC) style workflow like banks and financial institutions
*  Social media accounts can be persuasive if they have significant, long term, consistent posts
*  Some credit reporting agencies have a pretty clear idea of everyone

The three biggest complications:

1.  People move
2.  Districts are redrawn
3.  People lie

The best solution is probably very imperfect but allow self-regulation and self-reporting while storing as little content as possible long-term. The Notary-style plan can use a reputation mapping and cross-reference approach to mitigate the risks.

The amount of resources put into lobbying and campaigns means that this application will be the target of some very large scale concerted efforts to undermine it. 

### Bottom line

As we begin to gain mementum on this effort, community help is crucial. Development takes time and good development requires more eyes and energy than the organization currently has. 

[Please contribute](/contribute)

