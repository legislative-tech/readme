---
title: "Contribute to Legislative.Tech"
description: "There are many ways to contribute!"
---
We love that you're here looking for a way to contribute to the Legislative.Tech efforts. Below are listed various ways to participate, contribute to the mission. 

### Submit source code

The best way to help move Legislative.Tech forward is to submit fixes and features to the software that we are creating. The [repositories](https://gitlab.com/legislative-tech) are open to the public where they can be forked, modified, and changes submitted back as a merge request. Please follow the documentation in the readme and contributing guides to have the best experience. 

### Suggest data sources or modeling techniques

The second best way to contribute is to [create an issue in one of the repositories](https://gitlab.com/groups/legislative-tech/-/issues) to suggest a way to solve a problem. These may be related to data is collected or processed, though it could be about anything. Submitting an issue doesn't require that you follow-up with an implementation, though that would be nice. 

### Share design, psychology, or other expertise

You can still contribute even if you have expertise in an area that doesn't generate source code. [Create an issue](https://gitlab.com/groups/legislative-tech/-/issues) related to topics such as:

- Design
- Psychology
- Sociology
- Political science
- Economics
- or another relevant field

This project spans many disciplines so incorporating the current understanding of each will deliver better results.  