# Governance Principles

These are not offical policies or bylaws. These are the ideals that should guide Legislative Tech to being impactful and staying impactful. 

## I. Open source

In addition to making the software open source, and running the organization openly, LT will use as many open source tools as possible. Where these tools are deficient, LT will enhance them. If the tools need to be replaced by proprietary tools, it needs to be significant. 

### 0. Legislative.Tech open sources everything produced

That's it. It's all open. 

### 1. Legislative.Tech uses open source software to make software

As much as possible, Legislative.Tech prefers the most open licenses possible as building blocks for the software it creates. There are limits to this which will be deliberated over and decided upon. The default is to pick the most open and free option and then if a compelling reason exists to use a less-open or closed-source item, it needs to be reviewed and approved. 

### 2. Legislative.Tech prefers open source vendors

Where the service or organization requires software to handle source control, hosting, logging, customer relationship management, or other needs, Legislative.Tech will use the most open vendor possible. This may still be paid, such as Mattermost or GitLab for collaboration and SDLC. 

### 3. Legislative.Tech will use cloud services 

Some functionality for the services to be rendered will be dramatically better if hosted in a cloud. The cloudy nature includes closed source and constrained licenses. This is a tradeoff that can be quantified and decided to proceed. It also creates an opportunity to track the benefits and provide retrospectives about whether it was a good or bad idea and adjust course. 

### 4. Legislative.Tech will still use Excel

It's unavoidable... 'ya know? Spreadsheets just do so much. 

## II. Data handling

There are multiple types of Data that will be collected, processed, rendered, and generated by the software and organizational activity. 

### 0. Ownership

The content submitted by users is owned by the individuals who are contributing it to the system. No transfer of ownership of that content should be included in any user license agreement or privacy policies. 

Data about the users operating the system which is generated by the system and used by the developers and operators of the system will be owned by the Legislative.Tech organization or the provider if different than Legislative.Tech. Third party operators should keep to these guidelines as much as possible, and some aspects may be included in the open source license. 

### 1. Monetizing

The data is never for sale. This is a non-profit that is going to be handling sensitive data. Any user patterns or aggregations must be put to the mission or destroyed. Personal data is to be handled as toxic, risky, and sensitive. Deletion is okay if it protects users. 

### 2. Privacy 

Telemetry and user analytics will be part of the software and services rendered since they allow for the service to improve. As soon as the relevance fades, data shoud be pruned. Archives deteled. 

Backups should be localized to small units of the system, rather than taking massive backups with all of the data tied together. The optimal storage for backups would be encrypted by keys that the users have rather than the central system, though this is an engineering problem which is difficult. 

### 3. Disclosure

All sensitive information should be protected from disclosure. The best way to protect it is to delete it. If the organization needs to retain it for presentation in the application or model training, it must be encrypted and protected. Similar to privacy, distributing the encryption mechanisms to users or districts will reduce the disclosure risk significantly. 

### 4. Staleness

Data that users enter into the system must be given an expiration and only retained if the user states to do so. Some examples:

a. User enters story about their family into myrep related to the impact of a law.  This data is relevant to the current representative for the current legislative session.  Upon the end of that session, the user may be asked to retain or update the story if their problem persists or was explicitly resolved. If the user takes no action, the story is purged. 
b. A feature change is planned with A/B tests showing somee users a card view of policies rather than tabular view. The feedback is clear that tabular is superior so the implementation is completed and tabular view is rolled out to everyone. The A/B testing analytical data for that feature is purged. 
c. System operations are running fine for 60 days. Recorded telemetry and analytics recorded before that that 60-day period (not tied to any specific experimentation) is purged. 

### 5. Locality

Data locality is a concept where the system architecture eschews a large single backend feeding multiple systems in order to have each system own the data it is responsible for collecting, processing, and presenting to users. This will be used extensively in Legislative.Tech applications in order to limit exposure by providing users the ability to rescind their contributions at any time. 

For example, an expected use of myrep.app is to collect firsthand stories of how existing policies help or hurt individuals in a community. In order to classify, rank, and present these to the representative and community members, the stories need to be processed. This processing can take many forms, but an obvious approach would be natural language processing (NLP). The submission of the story would constitute a data storage event in MyRep.app (local district DB if we can) which is then provided to the NLP system and evaluated against the current model. All of the data is provided to the NLP queue and an identifier with classification information is given back to the MyRep.app instance. That story should not be shipped to a warehouse, though the model may be updated based on what it learned from the story. 

### 6. Vendors data policies

Vendors should respect these policies and guidelines and provide a mechanism for purging old data. Any vendor that cannot meet these guides should not be provided sensitive data.  

For example, if a vendor refuses to make data stale and expire it from their systems, they cannot process or store constituent stories since those are the most sensitive data and must remain under the constituent's control. 

## III. Code contributions

The only way for Legislative.Tech to succeed in the mission is to receive and benefit from the massive online community of potential contributors. 

### 0. Paying for contributions

This is a tough one that will need to be solved. Payment for contributions of source code is not unreasonable, just as a bug bounty or security program provides payouts for external parties who provide value to the organization. 

The IRS rules will be the main source of guidance for this. 

### 1. Employee contributions

Some developers may be employed by the organization to enhance the code and improve the products. Volunteer work is appreciated, but professionals responsible for the overall quality of the products will be necessary. 

### 2. Detailed guidelines

Each product will have detailed guidelines for what sort of contributions are anticipated and what rules to follow to get acceptance. Some software may be used for other purposes (generic decision support) so contributions that don't reach closer to the mission must be carefully considered. Technical debt to maintain code that doesn't serve the purpose here may lend itself to a community fork for that purpose which is out of the purveiew and guidance of Legislative.Tech. 

### 3. Nefaious contributions

If a contribution would severely damage the software, it will obviously be rejected. Reports may be published to share if a supply chain attack or other attempt is made to undermine trust in the system. 

## IV. Organization contributions

The openness of the organization will increase scrutiny and provide external parties who care about the organization to submit their own changes. 

Operating out of a handbook-style repository (this readme repository, for example) will be an effective way to collect these outside suggestions and consider them. 

### 1. Process

1. Fork the repository, make a change to the bylaws, governance documents, web page, or whatever item needs updates. 
2. Make the changes in your local branch
3. Create a merge request from your local branch to the main branch
4. Receive feedback related to compliance and applicability 
5. Make adjustment as requested or withdraw merge request if no-longer-applicable
6. Merge request will be merged when it is approved if minor, or after board approval if important

## V. Algorithms and ML 

The technology created by legislative tech will create and make use of existing allgorithms to empower a broader and clearer understanding of the constituents. There are ethical implicstions when the robots elevate and squash people's messages. 

### 0. Transparency reduces damage 

Classifications will be made visible to the users and aggregates can be viewed by the community. In situations where the individual feels a classification mistake was made, they can report and/or override the classification. These will be transparent to the representstive and the feedback csn be used to enhance the algorithms. 

### 1. Machine learning 

Where machine learning is used, its important for it to be able to describe itself. If problematic patterns emerge that would create or reinforce echo chambers, these need to be mitigsted. The same goes for minimized or hidden content. 

### 2. Ethics of questions and prompts

Since the processed content will most often come from a user submitting a story or opinion, it is important to ask the question or provide the prompts in a neutral fashion. Heavily leading or inflamatory language in the question will exascerbate tribalism rather than increasing alignment. 

### 3. Ethics of classifications

The algorithms are intended to classify content such that a lot of messages and stories can be mapped to upcoming laws and policy questions. Prohibiting "party labels" in classification will do a lot. Avoiding "progressive", "conservative", and "regressive" will also help. These are too generic to be informative. 

The best types of classifications would be those that show relative priorities of seemingly conflicting priorities. Often these manifest as "tax more" vs "spend on progrsm X" but even that misses the point. The cost of a program ks certsinly a concern, but new taxes aren't the only way to pay for a program. 

An example could be a story is processed about a family who has had recent medical issues and unemployment and now has uncertain housing. If applied to landlord rights bs tenant rights, the outcome will be punishing another constituent who is renting a property. If applied to an unemployment benefit expansion, they would be for increased coverage there, and the damage would be spread across a wider population. Both of those are downstream consequences of a healthcare issue, though. Ultimately, it is a higher priorotiy for the district to help the root cause. Depending on the condition, it could be best served bu better acute care or county health initiatives. Maybe even environmental or job safety. 

In that example, we can look at the complex system of failures that lead to the individual suffering and see how to prevent others frkm falling jnto the trap. This doesnt help the people in the trap, so a temporary measure to help them, or even ad hoc community action could handle the situation. A voluntary contribute of $5 by 5% of 200,000 would be $50,000 which could likely stabilize multiple families without creating a new tax. If the scale of the problem is larger than a community effort, then legislation should be considered. 
