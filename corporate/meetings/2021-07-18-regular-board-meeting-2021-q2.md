# Regular Board Meeting 2021-Q2 close

Uneventful quarter due to IRS processing delays and a busy time for the only board member and volunteer. 

## Quarter in brief

- Still waiting on 501(c)(3) status to move forward since service providers like Banks and Tech Vendors need that for discounts
- actual development progress stalled but marketing efforts continue in blog post and CFP submissions 

## Q2 output

- focus on myrep.app cryptography but no solution 
- identified some better ways to design the application for easy contribution from outsiders
- found an NLP template to follow for text input handling
- decided it's too early to give a talk 

## Planning for Q3
 
- ingest representative's legislative votes and bill sponsorship 
- doesnt requrie authentication or encryption since it's all public 

## Q2 Transactions

1. no transactions

## Approved by Mike Terhar, President
