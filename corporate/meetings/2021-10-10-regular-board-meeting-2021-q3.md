# Regular Board Meeting 2021-Q3 close

A couple of interesting IRS interactions this quarter helped crystalize the ideal future path forward for Legislative.Tech and its public service goals.

## Quarter in brief

- Emerging methods for handling shared trust are a compelling way to tie the system interactions with real-life humans: ethereum-style blockchain contracts and NFTs
- Learned that the charitable class that we are serving is radicalized people and their family, friends, and neighbors.
- Social media leaks and sentiment are exposing the problems that Legislative.Tech is looking to help tackle

## Q3 output

- corrected spelling in the Q2 meeting minutes
- satisfied the IRS's curiosity about Legislative.Tech's purpose
- holding off on many items until 501(c)(3) status is granted

## Planning for Q4

- identify board members who are passionate and engaged
- investigate accounting consultants and services
- ingest representative's legislative votes and bill sponsorship 
- doesnt requrie authentication or encryption since it's all public 

## Q3 Transactions

1. no transactions

## Approved by Mike Terhar, President
