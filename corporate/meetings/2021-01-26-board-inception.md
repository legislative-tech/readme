# Preliminary meeting

The board consists of only one person at organization inception. As soon as the IRS provides the 501(c)3 confirmation, a bank account will be created. 

At that point initial donations will be collected and vendor relationships will be forged. 

## Organizational meeting 

A few board members need to be identified and invited to join. Depending on interest, may be readonable to appoint officers. 

## Initial transactions

1. $75.00 Virginia State Corporation Commission articles of incorporation 
1. $25.00 VA SCC articles of correction  
1. $275.00 IRS form 1023-ez application fee 

## Approved by Mike Terhar, President

