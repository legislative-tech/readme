# Regular Board Meeting 2021-Q1 close

Uneventful quarter due to IRS processing delays and a busy time for the only board member and volunteer. 

## Quarter in brief

- IRS progress seems slow or backwards since the 1022-ez date went back to November 2020 from January 
- waiting on 501(c)(3) status to move forward seems like it may be detrimental
- actual development progress stalled but marketing efforts continue in blog post and CFP submissions 

## Q1 output

- reviewed technical landscape for data handling and found little help
- tools like [Census](https://getcensus.com) are more about operational metrics and decision support than product data processing 
- google has some good data processing tools but it may create problematic inertia

## Planning for Q2

- make progress on the myrep app story ingest and processing engine 
- ingest representative's legislative votes and bill sponsorship 
- give a talk on Legislative.Tech at GitLab Commit

## Q1 Transactions

1. no transactions

## Approved by Mike Terhar, President