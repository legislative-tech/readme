banner:
  title: "Let's fix representative democracy together."
  image: "/images/bradford-uk.jpg"
  content: "Contacting your representative is frustrating. Today's tribalism drives vilification and fear. Americans mostly agree on the important issues. Only the differences are highlighted. How do we align priorities and hold representatives accountable?"
  button: 
    enable : true
    label : "Find out more"
    link : "about"
    #  button:
    #enable : true
    #label : "Contribute"
    #link : "contribute"

feature:
  enable: true
  title: "Achieving alignment"
  feature_item:
    # feature item loop
    - name: "Prioritize"
      icon: "ti-medall" # "ti-direction-alt"
      content: "Social and collaborative prioritization at the local level."
    - name: "Constituents only"
      icon: "ti-id-badge" # "ti-map"
      content: "Trolling and interference reduced by participation limits."
    - name: "Open source"
      icon: "ti-linux"
      content: "Source code, algorithms, and operating information is available for public review."
    - name: "Algorithms"
      icon: "ti-ruler-alt"
      content: "The same methods that drive divisive content in social media, repurposed for coalition building."

######################### Service #####################
service:
  enable: true
  service_item:
    - title: "Local politics have the greatest impact on individuals."
      images: 
      - "images/neighbors-sf.jpg"
      content: "The lives of most people are much more directly impacted by local politics than by the national level. News and social media often paint the opposite picture, pulling attention and resources away from the local level."
      button:
        enable: true
        label: "Contribute"
        link: "contribute"
    - title: "Political parties create a false dichotomy"
      images:
        - "images/false-dichotomy.jpg"
      content: "Parties have coalesced around radical positions on hot button issues. Splitting votes based on extreme issues creates a system that doesn't work for anyone."
    - title: "The power of online communities"
      images:
        - "images/zoom-community.jpg"
      content: "Online communities enable greater participation by people who lack transportation or control of their schedules. Allowing asynchronous engagement lets more people be heard."
    - title: "Data pipelines and machine learning"
      images: 
        - "images/cpu-close.jpg"
      content: "Private industry has turned these technologies on their users to achieve optimizations, though many are driven by growth or revenue. The same methods can serve altruistic goals if the right governance and oversight are established."


################## Screenshot ########################
screenshot:
  enable : true
  title : "MyRep.app is the central collaboration platform driving the positive changes."
  image : "images/screenshot.png"

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Ready to get started?"
  image : "images/tree-hands.jpg"
  content : "The technology gets better faster as more people participate. "
  button:
    enable : true
    label: "Contribute"
    link: "contribute"
